from conexion import Conexion
from logger_base import logger

class CursorPool:
    def __init__(self):
        self.__conn = None
        self.__cursor = None
    
    #Inicio de la sintaxis with
    def __enter__(self):
        logger.debug(f'Inicio de with metodo __enter__')
        self.__conn = Conexion.obtener_conexion()
        self.__cursor = self.__conn.cursor()
        return self.__cursor
    
    #fin del bloque with
    def __exit__(self, exception_type, exception_value, exception_traceback):
        logger.debug(f'Se ejecuta metodo __exit__')
        if exception_value:
            self.__conn.rollback()
            logger.debug(f'Ocurrio una excepcion: {exception_value}')
        else:
            self.__conn.commit()
            logger.debug(f'Commit de la transaccion...')
        #Cerramos el cursor
        self.__cursor.close()
        #Devolvemos la conexion al pool
        Conexion.liberar_conexion(self.__conn)
        

if __name__ == '__main__':
    #Obtenemos un cursor a partir de la conexion del pool
    #with ejecuta __enter__ y finaliza con __exit__
    with CursorPool() as cursor:
        cursor.execute('SELECT * FROM persona')
        logger.debug(f'Listado de personas')
        logger.debug(cursor.fetchall())